import java.util.Date;
import java.io.*;
import java.net.*;

public class TimeServer{
    public static void main(String args[]){

	final int PORT;
	ServerSocket serverSocket = null;
	
	if (args.length != 1) {
	    System.out.println("Usage java TimeServer PORT");
	    System.exit(1);
	}

	PORT = Integer.parseInt(args[0]);

	try{
	    serverSocket = new ServerSocket(PORT);
	} catch (IOException ex){
	    System.out.println("Error creating server socket: " +
			       ex.getMessage());
	    System.exit(2);
	}

	while (true) {

	    try {

		Socket socket = serverSocket.accept();
		PrintWriter netOutput =
		    new PrintWriter(socket.getOutputStream());

		Date now = new Date();
		netOutput.println(now);
		netOutput.flush();

		socket.close();
	    } catch (IOException ex) {
		System.out.println(ex.getMessage());
	    }
	}
    }
}
